/***********************************************************************************
  Filename: light_switch.c

  Description:  
	This application function as a radio receiver/transmitter.
	It can also program the MSP430.  
  

***********************************************************************************/

/***********************************************************************************
* INCLUDES
*/
#include <hal_lcd.h>
#include <hal_led.h>
#include <hal_joystick.h>
#include <hal_assert.h>
#include <hal_board.h>
#include <hal_int.h>
#include "hal_mcu.h"
#include "hal_button.h"
#include "hal_rf.h"
#include "util_lcd.h"
#include "basic_rf.h"
#include "hal_timer_32k.h"
#include "hal_uart.h"
#include "CRC.h"
#include <stdbool.h>
#include "adc.h"
#include "hal_flash.h"
#include "hal_dma.h"
/***********************************************************************************
* CONSTANTS
*/
//Device Role - only one must be defined
#define PROGRAM_END_DEVICE	1

// Application parameters
#define RF_CHANNEL                11	//25      // 2.4 GHz RF channel

// BasicRF address definitions
#define PAN_ID                0x2007
#define SWITCH_ADDR           0x2520
#define LIGHT_ADDR            0xBEEF
#define APP_PAYLOAD_LENGTH        100
#define LIGHT_TOGGLE_CMD          0XDD

// Application states
#define IDLE                      0
#define SEND_CMD                  1

#define ANTENNA_A					0
#define ANTENNA_B					1

#define IDLE                      0
#define POLL_UART_DATA	          0xAA

#define RF_TX_PERIOD			 32     //16->0.512ms	// 32->0.97ms	// 313 -> 10ms
#define STATS_LENGTH			100
#define RF_HEADER_BYTES			10

#define BSL_MODE_TIMEOUT		9000*2
#define FIRST_COMM_TIMEOUT		10000   // 10000 = 5sec   //35000
#define SECOND_COMM_TIMEOUT             63000   // 60000 = 30 sec

/**********************************************************
Baud rate (bps) UxBAUD.BAUD_M UxGCR.BAUD_E Error(%)
2400 				59 			6 			0.14
9600 				59 			8 			0.14
57600 				216 		10 			0.03
115200 				216 		11 			0.03
230400 				216 		12 			0.03

Commonly used Baud Rate Settings for 32 MHz System Clock*/

// BAUD RATE 115,200
#define uartBaudM		216
#define uartBaudE		11

// BAUD RATE 9600
#define uartBaudM_9600		59
#define uartBaudE_9600		8

#define RX_DATA_SIZE		50

// PACKET HEADER DEFINES
#define HOST_PACKET_HEADER	0X46
#define WTTTS_PACKET_HEADER 	0X29
#define ENTER_BSL_MODE		0X90
#define BSL_PROGRAM_DATA	0X92
#define BSL_MODE_ENTERED	0X94
#define APP_MODE_ENTERED	0X96
#define ENTER_APP_MODE		0X98

#define	BSL_MODE_ENTERED_LENGTH	14
#define	APP_MODE_ENTERED_LENGTH	13

#define TX_ACK			0XB3
#define TX_NACK			0XDC
#define ENTER_PM_3		0XA7
#define POWER_MODE3		0x03

#define COMMAND			0X53
#define TX_DATA			0X29
#define RADIO_CHANGE_CHANNEL	0XA9
#define RADIO_CHANGE_POWER	0XA6
#define RADIO_RCVR_ON           0XA1
#define RADIO_RCVR_OFF          0XA3
#define RADIO_RCVR_ON_TIMED     0XA2
#define RADIO_RCVR_OFF_TIMED    0XA4
#define RADIO_SET_ADDRESS       0XA5
#define RADIO_SET_PAN_ID        0XA8
#define RADIO_SET_TIMEOUT       0XCA

#define RESET_TIMES_BEFORE_SLEEP        2



#define ADC_INTERNAL_REF        0x00
#define ADC_RESOLUTION_12BIT    0x30
#define ADC_CHANNEL_1           0x00
#define ADC_CHANNEL_2           0x01

#define CW_MIN_CHANNEL          7
#define CW_MAX_CHANNEL          90
#define NBR_RF_SAMPLES          100
#define LED_QUICK_FLASH         125
#define LED_NORMAL_FLASH        250
#define SHOW_RESULT_TIME        5000

#define CAL_JUMPER_IN_PORT      0
#define CAL_JUMPER_IN_PIN       2
#define CAL_JUMPER_OUT_PORT     0
#define CAL_JUMPER_OUT_PIN      3

/***********************************************************************************
* LOCAL VARIABLES
*/


static uint8 pTxData[APP_PAYLOAD_LENGTH];
static basicRfCfg_t basicRfConfig;


static volatile uint8 appState;


static unsigned char RxBuffer[RX_DATA_SIZE];
static uint8	RxBufIndex; 
static uint32 mSecCounter1,mSecCounter2,mSecCounter3 ;
static uint8  bBSLMode;


uint16 wPacketLen;



static uint16 cnt16bit;
static int16  adcResult[2];
static int16   freqCoeffWrite[128];
static int16   freqCoeffRead[128];
//const  uint32  FREQ_PARAM_ADD   = 0x20000;
const  uint32  FREQ_PARAM_ADD   = 0x8000;
typedef struct {
    int16 calibration0db[128];
    int16 calibration20db[128];
} CALIBRATION_PARAMS;



CALIBRATION_PARAMS      calParam1;
CALIBRATION_PARAMS      calParam2;

int16 calibration12db[128];

int16      testParams[128];
int16      testParams[128];
int16      testPassLimit[128];
int16      testFailLimit[128];


const int32 ADC_COEFF_UV           = 805;
const int32 ADC_PS_COEFF_UV        = 305;
const int16 CAL_OPEN_CIRCUIT_MV    = 3200;
const int16 CAL_SHORT_CIRCUIT_MV   = 100;
const int16 CAL_20DB_LOW_MV        = 185;
const int16 CAL_20DB_HIGH_MV       = 195;
const int16 TEST_ANT_LOW_MV        = 1600;
const int16 TEST_ANT_HIGH_MV       = 1700;
const int16 TEST_LOAD_LOW_MV       = 100; //            //152;
const int16 TEST_LOAD_HIGH_MV      = 162;


#ifdef SECURITY_CCM
static uint8 key[]= {
    0xdf, 0xd0, 0xde, 0xd1, 0xdd, 0xd2, 0xdc, 0xd3,
    0x3d, 0xcd, 0x2d, 0x43, 0x87, 0x0f, 0x19, 0x82,
};
#endif

/***********************************************************************************
* LOCAL FUNCTIONS
*/
//static void appSwitch();
static void appTimerISR(void);
static void appConfigTimer(uint16 cycles);


void rRxBufGetAllData( char *buf);
void RxBufPut( char data);
void Uart1EnableRxInterrupt(void);
void RxBufReset(void);

bool SendRfPacket(uint8 *pData, uint16 wPackLen);
void ParseRfPAcket(uint8 *pData);
uint8 WriteProgramDataResp(uint8 *pData);
void SendDataToMcu(unsigned char* TxBuf, unsigned short TxBufLength);
void PowerDownMode(void);



// antenna tester functions.
void mainAntennaTesterTask(void);
bool isCalJumperPresent(void);
void setCwFrequency( uint8 channel);
void stopCwMode(void);
void calibrateZero_db(void);
void calibrate20_db(void);
bool validateCalParams(void);
int16 measureDC_CalInMilivolts(void);
int16 measureReturnLossInMilivolts(void);
void calibrationRoutine(void);
void testRoutine(void);
void testReturnLoss(void);
int16 calculate15db(uint8 channel);
int16 calculate12db(uint8 channel);
int16 measurePowerSupplyInMilivolts(void);
void saveCalParamsInFlash(void);
void readCalParamsFromFlash(void);
void calibrate12_db(void);
void LedTestTask(uint8 bState);

// test functions 
void TestFlashReadWrite(void);
void testCWfrequency(void);
void MeasureReturnLoss(void);
void basicTestTask(void);


/***********************************************************************************
* @fn          main
*
* @brief       This is the main entry of the "Light Switch" application.
*              After the application modes are chosen the switch can
*              send toggle commands to a light device.
*
* @param       basicRfConfig - file scope variable. Basic RF configuration
*              data
*              appState - file scope variable. Holds application state
*
* @return      none
*/
void main(void)
{
    appState = IDLE;
    int16 result16bit;

    IO_ADC_PORT0_PIN(ADC_AIN0, IO_ADC_EN);
    IO_ADC_PORT0_PIN(ADC_AIN1, IO_ADC_EN);
      
      
    // Config basicRF
    basicRfConfig.panId = PAN_ID;
    basicRfConfig.channel = RF_CHANNEL;
    basicRfConfig.ackRequest = TRUE;
#ifdef SECURITY_CCM
    basicRfConfig.securityKey = key;
#endif
		
    // Initalise board peripherals
    halBoardInit();
    appConfigTimer(RF_TX_PERIOD);
    halTimer32kIntEnable();
#if (defined HAL_DMA) && (HAL_DMA == TRUE)
  // Must be called before the init call to any module that uses DMA.
  HalDmaInit();
#endif
  
  
  mainAntennaTesterTask();
  
//    TestFlashReadWrite();
//    MeasureReturnLoss();
//    testCWfrequency();

    
    // Initalise hal_rf
//    if(halRfInit()==FAILED) {
//      HAL_ASSERT(FALSE);
//    }

   // Role is undefined. This code should not be reached
    HAL_ASSERT(FALSE);
}



static void appConfigTimer(uint16 cycles)
{
    halTimer32kInit(cycles);
    halTimer32kIntConnect(&appTimerISR);
}

/***********************************************************************************
* @fn          appTimerISR
*
* @brief       32KHz timer interrupt service routine. Signals PER test transmitter
*              application to transmit a packet by setting application state.
*
* @param       none
*
* @return      none
*/
static void appTimerISR(void)
{
    appState = POLL_UART_DATA;
    if(mSecCounter1)
        mSecCounter1--;
    if(mSecCounter2)
        mSecCounter2--;
    if(mSecCounter3)
        mSecCounter3--;        
}




void PowerDownMode(void)
{
    // Put MCU to sleep. 
    halIntOff();
    SLEEPCMD |= POWER_MODE3;
    PCON = 0x01;
}


/*****************************
 Antenna tester funcions
******************************/
void mainAntennaTesterTask(void)
{
    bool testModeVar = true;
//    if(testModeVar == false)
    // To enter calibration mode,
    // connect pins 8 and 9 on P1 with a jumper 
    if(isCalJumperPresent()== true)     
    {
        calibrationRoutine();
    }
    else        // if there is no jumper it enters in test mode.
    {
        testRoutine();
    }    
}


/******************************************************************************
* @fn  isCalJumperPresent
*
* @brief
*      This function checks if the calibration jumper is present
*      
*
* @param none
*           
*           
* @return bool true if present, false if not
*          
*
******************************************************************************/
bool isCalJumperPresent(void)
{
    bool bResult;
    MCU_IO_INPUT(CAL_JUMPER_IN_PORT, CAL_JUMPER_IN_PIN, MCU_IO_PULLDOWN);
    MCU_IO_OUTPUT(CAL_JUMPER_OUT_PORT, CAL_JUMPER_OUT_PIN, 1);
    
    bResult = false;
    if(MCU_IO_GET(CAL_JUMPER_IN_PORT, CAL_JUMPER_IN_PIN))
    {
        bResult = true;
    }
    else
    {
        bResult = false;
    }
    
    MCU_IO_INPUT(CAL_JUMPER_IN_PORT, CAL_JUMPER_IN_PIN, MCU_IO_PULLDOWN);
    MCU_IO_INPUT(CAL_JUMPER_OUT_PORT, CAL_JUMPER_OUT_PIN, MCU_IO_PULLDOWN);
    
    return bResult;
}


/******************************************************************************
* @fn  setCwFrequency
*
* @brief
*      This function sets the CW frequency.
*      
*
* @param uint8 channel
*           The CW carrier shall be in the range of 2401 to 2484 MHz in 1 MHz steps.
*           The carrier frequency fC, in MHz, is given by fC = (2394 + FREQCTRL.FREQ[6:0])
*           So the channel shall be a number from 7 to 90.             
*
* @return none
*          
*
******************************************************************************/
void setCwFrequency( uint8 channel)
{
    if(channel < CW_MIN_CHANNEL)
        return;
    if(channel > CW_MAX_CHANNEL)
        return;
    
    FREQCTRL = channel;
    RFD = 0xAA;
    FRMCTRL0 = 0x43;
    MDMTEST1 = 0x18;
    ISTXON();
   
}

/******************************************************************************
* @fn  stopCwMode
*
* @brief
*      This function stops the CW transmission and sets the radio in receive mode.
*      
*
* @param none
*           
*
* @return none
*          
*
******************************************************************************/
void stopCwMode(void)
{
    basicRfReceiveOn();
}


/******************************************************************************
* @fn  saveCalParamsInFlash
*
* @brief
*      This function saves the calibration parameters in the flash memory 
*      
* @param none
*           
* @return none
*
******************************************************************************/
void saveCalParamsInFlash(void)
{
    HalFlashErase(FREQ_PARAM_ADD / HAL_FLASH_PAGE_SIZE);
    
    //    HalFlashWrite(FREQ_PARAM_ADD / HAL_FLASH_WORD_SIZE, (uint8 *)&calParam1, sizeof(calParam1) / HAL_FLASH_WORD_SIZE);
    HalFlashWrite(FREQ_PARAM_ADD / HAL_FLASH_WORD_SIZE, (uint8 *)calibration12db, sizeof(calibration12db) / HAL_FLASH_WORD_SIZE);
}

/******************************************************************************
* @fn  readCalParamsFromFlash
*
* @brief
*      This function reads the calibration parameters from the flash memory 
*      
* @param none
*           
* @return none
*
******************************************************************************/
void readCalParamsFromFlash(void)
{
//    HalFlashRead(FREQ_PARAM_ADD / HAL_FLASH_PAGE_SIZE, FREQ_PARAM_ADD % HAL_FLASH_PAGE_SIZE, (uint8 *)&calParam1, sizeof(calParam1) );
    HalFlashRead(FREQ_PARAM_ADD / HAL_FLASH_PAGE_SIZE, FREQ_PARAM_ADD % HAL_FLASH_PAGE_SIZE, (uint8 *)calibration12db, sizeof(calibration12db) );
}

/******************************************************************************
* @fn  validateCalParams
*
* @brief
*      This function validates the calibration parameters. 
*      
* @param none
*           
* @return true/false. 
*
******************************************************************************/
bool validateCalParams(void)
{
    uint8 channel;
    for(channel = CW_MIN_CHANNEL; channel<=CW_MAX_CHANNEL; channel++)
    {
        if(calibration12db[channel]==0xFFFF)
        {
            return false;
        }
    }
    return true;
}

/******************************************************************************
* @fn  calibrateZero_db
*
* @brief
*       This function turns on the RF and measures the voltage from the RF detector, starting at the bottom of the 
*       frequency band and going in 1 MHz steps.  At each step it takes a large number of samples and calculates an average.  
*       It stores the values in an array associated with the calibration standard.  
*      
* @param none
*           
* @return none
*
******************************************************************************/
void calibrateZero_db(void)
{
    uint8 channel;
    
    mSecCounter1 = LED_NORMAL_FLASH;
    for(channel = CW_MIN_CHANNEL; channel<=CW_MAX_CHANNEL; channel++)
    {
        if(mSecCounter1 == 0)
        {
            HAL_LED_TGL_2();
            HAL_LED_TGL_3();
            mSecCounter1 = LED_NORMAL_FLASH;
        }
        setCwFrequency(channel);
        halMcuWaitUs(100);
        calParam1.calibration0db[channel] = measureReturnLossInMilivolts();
        stopCwMode();
    }    

    HAL_LED_SET_2();
    HAL_LED_SET_3();
    
}

/******************************************************************************
* @fn  calibrate20_db
*
* @brief
*       This function turns on the RF and measures the voltage from the RF detector, starting at the bottom of the 
*       frequency band and going in 1 MHz steps.  At each step it takes a large number of samples and calculates an average.  
*       It stores the values in an array associated with the calibration standard.  
*      
* @param none
*           
* @return none
*
******************************************************************************/
void calibrate20_db(void)
{
    uint8 channel;
     
    mSecCounter1 = LED_NORMAL_FLASH;
    for(channel = CW_MIN_CHANNEL; channel<=CW_MAX_CHANNEL; channel++)
    {
        if(mSecCounter1 == 0)
        {
            HAL_LED_TGL_3();
            HAL_LED_TGL_4();
            mSecCounter1 = LED_NORMAL_FLASH;
        }
        setCwFrequency(channel);
        halMcuWaitUs(100);
        calParam1.calibration20db[channel] = measureReturnLossInMilivolts();
        stopCwMode();
    }    
    
    HAL_LED_SET_3();
    HAL_LED_SET_4();
}



/******************************************************************************
* @fn  calibrateZero_db
*
* @brief
*       This function turns on the RF and measures the voltage from the RF detector, starting at the bottom of the 
*       frequency band and going in 1 MHz steps.  At each step it takes a large number of samples and calculates an average.  
*       It stores the values in an array associated with the calibration standard.  
*      
* @param none
*           
* @return none
*
******************************************************************************/
void calibrate12_db(void)
{
    uint8 channel;
    
    HAL_LED_CLR_1();
    HAL_LED_CLR_2();
    HAL_LED_CLR_3();
    HAL_LED_CLR_4();
    
    mSecCounter1 = LED_NORMAL_FLASH;
    for(channel = CW_MIN_CHANNEL; channel<=CW_MAX_CHANNEL; channel++)
    {
        if(mSecCounter1 == 0)
        {
            HAL_LED_TGL_2();
            HAL_LED_TGL_3();
            mSecCounter1 = LED_NORMAL_FLASH;
        }
        setCwFrequency(channel);
        halMcuWaitUs(100);
        calibration12db[channel] = measureReturnLossInMilivolts();
        stopCwMode();
    }    

    HAL_LED_SET_2();
    HAL_LED_SET_3();
    
}

/******************************************************************************
* @fn  measureDC_CalInMilivolts
*
* @brief
*      measure the DC voltage at the RF connector to determine the load, or open/short circuit.
*      
*      
* @param none
*           
* @return none
*
******************************************************************************/
int16 measureDC_CalInMilivolts(void)
{
    
    int16 i;
    int32 avgResult;
    int32 resultInMilivolts;    
    
    avgResult = adcSampleSingle(ADC_REF_AVDD, ADC_12_BIT, ADC_AIN1);   
    for(i = 0;i<10;i++)
    {
        
        adcResult[1] = adcSampleSingle(ADC_REF_AVDD, ADC_12_BIT, ADC_AIN1);
        avgResult = avgResult + adcResult[1];
        avgResult /= 2;
    }
    // only bits [14 .. 3] (bitmask 0x7FF8) are significant. shift right 3 bits
    avgResult >>=3;
    resultInMilivolts = avgResult * ADC_COEFF_UV; // calculate microvolts.
    resultInMilivolts /= 1000; // convert in milivolt
    
    return (int16)resultInMilivolts;
}


/******************************************************************************
* @fn  measureReturnLossInMilivolts
*
* @brief
*      measure the DC voltage at the output of the RF detector to determine the return loss.
*      
*      
* @param none
*           
* @return none
*
******************************************************************************/
int16 measureReturnLossInMilivolts(void)
{
    
    int16 i;
    int32 avgResult;
    int32 resultInMilivolts;    
    
    avgResult = adcSampleSingle(ADC_REF_AVDD, ADC_12_BIT, ADC_AIN0);   
    for(i = 0;i<NBR_RF_SAMPLES;i++)
    {
        
        adcResult[0] = adcSampleSingle(ADC_REF_AVDD, ADC_12_BIT, ADC_AIN0);
        avgResult = avgResult + adcResult[0];
        avgResult /= 2;
    }
    
    // only bits [14 .. 3] (bitmask 0x7FF8) are significant. shift right 3 bits
    avgResult >>=3;
    resultInMilivolts = avgResult * ADC_COEFF_UV; // calculate microvolts.
    resultInMilivolts /= 1000; // convert in milivolt
    
    return (int16)resultInMilivolts;
}


/******************************************************************************
* @fn  calibrationRoutine
*
* @brief
*       The Tester requires two calibration standards:  0 dB return loss (open circuit); and 20 dB return loss (unterminated 10 dB attenuator).  
*       When the Tester powers up in Calibration mode, the Tester shall determine which standard is applied by measuring the DC voltage.  
*       If the DC voltage is close to 3.3 V, the standard is open circuit.  (Acceptable range is >3.2 V)
*       If the DC voltage is close to 3.3 x 61/(1061) = 0.19 V, the standard is an unterminated 50 ohm 10-dB attenuator. (Acceptable range is 0.185  0.195 V)
*       If the DC voltage is neither of the above there is a calibration error.
*       When a valid standard has been detected, the Tester turns on the RF and measures the voltage from the RF detector, starting at the bottom (or top) of 
*       the frequency band and going in 1 MHz steps.  At each step it takes a large number of samples and calculates an average.  
*       It stores the values in an array associated with the standard.  While the calibration is in progress it rapidly flashes the LEDs associated with the standard.  
*       Once the calibration is complete the Tester turns the associated LEDs on continuously.  (Nothing further happens until the Tester is power cycled.  
*       In order to do the other standard, the power button must be released and the other standard applied before powering up again).
*      
* @param none
*           
* @return none
*
******************************************************************************/
void calibrationRoutine(void)
{
    int16 milivolts;
    
    mSecCounter1 = 0;
    
    while(1)
    {
        milivolts = measureDC_CalInMilivolts();
        
//        if((CAL_20DB_LOW_MV<milivolts)&&(milivolts<CAL_20DB_HIGH_MV))
//        if(CAL_20DB_LOW_MV<milivolts)
        if(CAL_SHORT_CIRCUIT_MV<milivolts)
        {
            calibrate12_db();
            // save the cal. parameters
            saveCalParamsInFlash();
            while(1);
        }
        else
        {
            if(mSecCounter1 == 0)
            {
                HAL_LED_TGL_2();
                HAL_LED_TGL_3();
                HAL_LED_TGL_4();
                mSecCounter1 = LED_QUICK_FLASH;
            }
        }
        
    }
    
    
}


/******************************************************************************
* @fn  testRoutine
*
* @brief
*       This function shall do the following:
*      Cal Verification Check:
*      When the Tester starts up in Test Mode, it first checks to see that there is valid data in both the Open Circuit and Attenuator calibration arrays.  
*      If it does not detect valid data (i.e. FFs) in both arrays it flashes all LEDs and does not proceed.
*      If it detects valid data in both arrays it proceeds to continuously check the DC voltage.
*      DC Check:
*      If the DC voltage is approximately 1.65 V, an antenna is present.  (Acceptable range is 1.60  1.70 V).  Proceed to Return Loss test.
*      If the DC voltage is approximately 0.157 V, a 50-ohm termination is present. (Acceptable range is 1.52  1.62 V).  Proceed to Return Loss test.
*      If the DC voltage is greater than 3.2 V, an open circuit fault has been detected. Indicate an Open Circuit fault and continue to check the DC voltage.
*      If the DC voltage is less than 0.1 V, a short circuit fault has been detected.  Indicate a Short Circuit fault and continue to check the DC voltage.
*      If the DC voltage is none of the above, an unknown fault has been detected.  Flash all LEDs and continue to monitor the DC voltage.
*      
* @param none
*           
* @return none
*
******************************************************************************/
void testRoutine(void)
{
    int16 milivolts;
    uint8 ledState = 0;
    
    mSecCounter1 = 0;
    readCalParamsFromFlash();
    if(validateCalParams()==false)
    {
        while(1)
        {
            if(mSecCounter1 == 0)
            {
                HAL_LED_TGL_2();
                HAL_LED_TGL_3();
                HAL_LED_TGL_4();
                mSecCounter1 = LED_QUICK_FLASH;
            }
        }
    }
    
    while(1)
    {
        milivolts = measureDC_CalInMilivolts();
//        if(((TEST_ANT_LOW_MV<milivolts)&&(milivolts<TEST_ANT_HIGH_MV))
//           || ((TEST_LOAD_LOW_MV<milivolts)&&(milivolts<TEST_LOAD_HIGH_MV)))
        if(CAL_SHORT_CIRCUIT_MV<milivolts)      // if it's not short circuit(100mV) continue with testing
        {
            ledState = 0;
            HAL_LED_CLR_1();
            HAL_LED_CLR_2();
            HAL_LED_CLR_3();
            HAL_LED_CLR_4();
            testReturnLoss();
            //while(1);
        }
        else
        {
            if(milivolts>CAL_OPEN_CIRCUIT_MV)
            {
//                if(mSecCounter1 == 0)
//                {
//                    HAL_LED_TGL_2();
//                    HAL_LED_TGL_3();
//                    mSecCounter1 = LED_QUICK_FLASH;
//                }
                ledState = 1;
            }
            else
            {
                if(milivolts<CAL_SHORT_CIRCUIT_MV)
                {
//                    if(mSecCounter1 == 0)
//                    {
//                        HAL_LED_TGL_3();
//                        HAL_LED_TGL_4();
//                        mSecCounter1 = LED_QUICK_FLASH;
//                    }
                    ledState = 2;
                }
                else
                {
                    ledState = 3;
                }
            }
            LedTestTask(ledState);
        }         
        
    }
    
}

/******************************************************************************
* @fn  testReturnLoss
*
* @brief
*      
*      
*      
* @param none
*           
* @return none
*
******************************************************************************/
void testReturnLoss(void)
{
    // TODO
    uint8 channel;
    bool bFailTest;
    bool bWarning;
    int16 slope;
        
    mSecCounter1 = LED_NORMAL_FLASH;
    for(channel = CW_MIN_CHANNEL; channel<=CW_MAX_CHANNEL; channel++)
    {
        if(mSecCounter1 == 0)
        {
            HAL_LED_TGL_3();
            mSecCounter1 = LED_NORMAL_FLASH;
        }
        setCwFrequency(channel);
        halMcuWaitUs(100);
//        slope = (calParam1.calibration0db[channel] - calParam1.calibration20db[channel])/20;
//        testPassLimit[channel] = calParam1.calibration0db[channel] - (15 * slope);
//        testFailLimit[channel] = calParam1.calibration0db[channel] - (12 * slope);

        testParams[channel] = measureReturnLossInMilivolts();
        halMcuWaitUs(50);
        testParams[channel] = measureReturnLossInMilivolts();
        stopCwMode();
    }    
    
    bFailTest = false;
    bWarning = false; 
    for(channel = CW_MIN_CHANNEL; channel<=CW_MAX_CHANNEL; channel++)
    {
       
        if(testParams[channel] >  calibration12db[channel])
        {
            bFailTest = true;
        }
//        else
//        {
//            if(testParams[channel] >  testPassLimit[channel])
//                bWarning = true;
//        }
    }  
    HAL_LED_CLR_1();
    HAL_LED_CLR_2();
    HAL_LED_CLR_3();
    HAL_LED_CLR_4();
        
    if(bFailTest == true)
    {
        // test fail - set leds RED1 and RED2 solid for about a xx second.
        mSecCounter2 = SHOW_RESULT_TIME;
        HAL_LED_SET_2();
        HAL_LED_SET_4();
        while(mSecCounter2);
    }
    else
    {
        
        // test pass - set GREEN led solid for about a xx second.
        mSecCounter2 = SHOW_RESULT_TIME;
        HAL_LED_SET_3();
        while(mSecCounter2);
        
    }
    
// clear all leds.    
    HAL_LED_CLR_1();
    HAL_LED_CLR_2();
    HAL_LED_CLR_3();
    HAL_LED_CLR_4();           
    
    
}

/******************************************************************************
* @fn  calculate15db
*
* @brief
*      calculates the 15 db pass limit in milivolts according to the formulas:
*       Slope(f) = (V_oc(f)  V_20dB(f) ) / 20 =  V/dB
*       V_pass(f) = V_oc(f)  ( Pass(f) x Slope(f) ) = V
*       
* @param none
*           
* @return none
*
******************************************************************************/
int16 calculate15db(uint8 channel)
{
    int16 ch15dbIn_mv;
    int16 slope_mv;
    slope_mv = calParam1.calibration0db[channel] - calParam1.calibration20db[channel];
    slope_mv  /= 20;
    ch15dbIn_mv = calParam1.calibration0db[channel] - (15*slope_mv);
    
    return ch15dbIn_mv;
}

/******************************************************************************
* @fn  calculate12db
*
* @brief
*      calculates the 15 db pass limit in milivolts according to the formulas:
*       Slope(f) = (V_oc(f)  V_20dB(f) ) / 20 =  V/dB
*       V_pass(f) = V_oc(f)  ( Fail(f) x Slope(f) ) = V
*       
* @param none
*           
* @return none
*
******************************************************************************/
int16 calculate12db(uint8 channel)
{
    int16 ch12dbIn_mv;
    int16 slope_mv;
    slope_mv = calParam1.calibration0db[channel] - calParam1.calibration20db[channel];
    slope_mv  /= 20;
    ch12dbIn_mv = calParam1.calibration0db[channel] - (12*slope_mv);
    
    return ch12dbIn_mv;
}




/******************************************************************************
* @fn  measurePowerSupplyInMilivolts
*
* @brief
*      measure the Power Supply voltage 
*      
*      
* @param none
*           
* @return none
*
******************************************************************************/
int16 measurePowerSupplyInMilivolts(void)
{
    
    int16 i;
    int32 avgResult;
    int32 resultInMilivolts;    
    
    avgResult = adcSampleSingle(ADC_REF_1_25_V, ADC_12_BIT, ADC_VDD_3);   
    for(i = 0;i<NBR_RF_SAMPLES;i++)
    {
        
        adcResult[0] = adcSampleSingle(ADC_REF_1_25_V, ADC_12_BIT, ADC_VDD_3);   
        avgResult = avgResult + adcResult[0];
        avgResult /= 2;
    }
// only bits [14 .. 3] (bitmask 0x7FF8) are significant. shift right 3 bits
    avgResult >>=3;
    resultInMilivolts = avgResult * ADC_PS_COEFF_UV; // calculate microvolts.
    resultInMilivolts /= 1000; // convert in milivolt
    resultInMilivolts *= 3; 
    
    return (int16)resultInMilivolts;
}


//// TEST FUNCTIONS
void TestFlashReadWrite(void)
{
    int16 i;
    
    for(i=0;i<128;i++)
    {
        freqCoeffWrite[i]=i;
        freqCoeffRead[i] = 0;
        calParam1.calibration0db[i] = i;
        calParam1.calibration20db[i] = i+20;
        calibration12db[i] = i+2;
    }
    
    HalFlashErase(FREQ_PARAM_ADD / HAL_FLASH_PAGE_SIZE);
    
//    HalFlashRead(FREQ_PARAM_ADD / HAL_FLASH_PAGE_SIZE, FREQ_PARAM_ADD % HAL_FLASH_PAGE_SIZE,(uint8 *) &calParam2, sizeof(calParam2) );
//    
//    HalFlashWrite(FREQ_PARAM_ADD / HAL_FLASH_WORD_SIZE, (uint8 *)&calParam1, sizeof(calParam1) / HAL_FLASH_WORD_SIZE);
//    
//    HalFlashRead(FREQ_PARAM_ADD / HAL_FLASH_PAGE_SIZE, FREQ_PARAM_ADD % HAL_FLASH_PAGE_SIZE, (uint8 *)&calParam2, sizeof(calParam2) );
    
    saveCalParamsInFlash();
    memset(calibration12db, 0, sizeof(calibration12db));
    readCalParamsFromFlash();
    validateCalParams();
}




void testCWfrequency(void)
{
    uint8 channel;
    
    if(halRfInit()==FAILED) {
        HAL_ASSERT(FALSE);
    }
    // Initialize BasicRF
    basicRfConfig.myAddr = SWITCH_ADDR;
    if(basicRfInit(&basicRfConfig)==FAILED) {
      HAL_ASSERT(FALSE);
    }
    basicRfReceiveOn();


	
    while(1)
    {        
    mSecCounter1 = LED_NORMAL_FLASH;
    channel = CW_MIN_CHANNEL;
    mSecCounter2 = 5000;
    setCwFrequency(channel);
    while( channel<=CW_MAX_CHANNEL )
    {
        if(mSecCounter1 == 0)
        {
            HAL_LED_TGL_2();
            HAL_LED_TGL_3();
            mSecCounter1 = LED_NORMAL_FLASH;
        }
        calParam1.calibration0db[channel] = measureReturnLossInMilivolts();
//        if(mSecCounter2==0)
        if(1)
        {
            stopCwMode();
            channel++;
            setCwFrequency(channel);
            //halMcuWaitUs(3000);
            halMcuWaitUs(10000);
            halMcuWaitUs(10000);
            halMcuWaitUs(10000);
            mSecCounter2 = 5000;
        }
    }    

    HAL_LED_SET_2();
    HAL_LED_SET_3();
    }
    
}

/******************************************************************************
* @fn  MeasureReturnLoss
*
* @brief
*      measure the Return loss of an externally applied signal. The Radio transmitter is off. 
*      
*      
* @param none
*           
* @return none
*
******************************************************************************/
void MeasureReturnLoss(void)
{
    uint8 channel;
    
    if(halRfInit()==FAILED) {
        HAL_ASSERT(FALSE);
    }
    // Initialize BasicRF
    basicRfConfig.myAddr = SWITCH_ADDR;
    if(basicRfInit(&basicRfConfig)==FAILED) {
      HAL_ASSERT(FALSE);
    }
    basicRfReceiveOn();

    mSecCounter1 = LED_NORMAL_FLASH;
    channel = 0;
	
    while(1)
    {        

    {
        if(mSecCounter1 == 0)
        {
            HAL_LED_TGL_2();
            HAL_LED_TGL_3();
            mSecCounter1 = LED_NORMAL_FLASH;
        }
        calParam1.calibration0db[channel] = measureReturnLossInMilivolts();
        halMcuWaitUs(10000);
    }    

    HAL_LED_SET_2();
    HAL_LED_SET_3();
    }
    
}

void LedTestTask(uint8 bState)
{
    static uint8 currentState;
    if(bState==0)
        return;
    
    if(currentState!=bState)
    {
        // clear all leds.    
        HAL_LED_CLR_1();
        HAL_LED_CLR_2();
        HAL_LED_CLR_3();
        HAL_LED_CLR_4();  
        
        //TO DO - SET UP the toggle period
    }
    
    currentState=bState;
    if(mSecCounter1 == 0)
    {
        switch(currentState)
        {
          case 1:
            HAL_LED_TGL_2();
            HAL_LED_TGL_3();
            mSecCounter1 = LED_QUICK_FLASH;
            break;
          case 2:
            HAL_LED_TGL_3();
            HAL_LED_TGL_4();
            mSecCounter1 = LED_QUICK_FLASH;
            break;
          case 3:
            HAL_LED_TGL_1();
            HAL_LED_TGL_2();
            HAL_LED_TGL_3();
            HAL_LED_TGL_4();
            mSecCounter1 = LED_QUICK_FLASH;
            break;
          default:            
            break;
        }
        
    }
    
}

void basicTestTask(void)
{
    int16 result16bit;
    while(1)
    {
        HAL_LED_TGL_1();               
        //        halMcuWaitMs(100);
        mSecCounter3 = 100;
        while(mSecCounter3);
        HAL_LED_TGL_2();
        //        halMcuWaitMs(200);
        mSecCounter3 = 200;
        while(mSecCounter3);        
        HAL_LED_TGL_3();
        //        halMcuWaitMs(300);
        mSecCounter3 = 300;
        while(mSecCounter3);        
        HAL_LED_TGL_4();
        //        halMcuWaitMs(400);
        mSecCounter3 = 400;
        while(mSecCounter3);        
        //        adcResult[0] = adcSampleSingle(ADC_REF_AVDD, ADC_12_BIT, ADC_AIN0);
        //        adcResult[1] = adcSampleSingle(ADC_REF_AVDD, ADC_12_BIT, ADC_AIN1);
        result16bit =  measurePowerSupplyInMilivolts();       
        result16bit = measureDC_CalInMilivolts();
        result16bit =  measureReturnLossInMilivolts();
        
    }
}
/****************************************************************************************
  Copyright 2007 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
***********************************************************************************/
