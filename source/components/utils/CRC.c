#include "CRC.h"

#define CRC_SEED 0xFFFF
unsigned int crc;

//-------------------------------------------------------------------
// Function: crcReset
//
void crcReset(void)
{
	crc = CRC_SEED;
}

//-------------------------------------------------------------------
// Function: crcNextByte
//
void crcNextByte(unsigned char byte)
{
	unsigned int x;
	x = ((crc >> 8) ^ byte) & 0xFF;
	x ^= x >> 4;
	crc = (crc << 8) ^ (x << 12) ^ (x << 5) ^ x;
}

//-------------------------------------------------------------------
// Function: crcGetValue
//
unsigned int crcGetValue(void)
{
	return crc;
}

//-------------------------------------------------------------------
// Function: crcGetLowByte
//
unsigned char crcGetLowByte(void)
{
	return (unsigned char)(crc & 0xFF);
}

//-------------------------------------------------------------------
// Function: crcGetHighByte
//
unsigned char crcGetHighByte(void)
{
	return (unsigned char)((crc >> 8) & 0xFF);
}
